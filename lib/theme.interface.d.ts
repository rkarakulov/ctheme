import { ISemanticPalette } from './palette/themePalette.interface';
import { PaletteName, FontSize, BackgroundType } from './theme.constant';
import { ISemanticFont } from './font/themeFont.interface';
export interface IContextTheme {
    backgroundType?: BackgroundType;
}
export declare type ITheme = {
    palette: {
        name: PaletteName;
        semantic: ISemanticPalette;
    };
    font: {
        name: FontSize;
        semantic: ISemanticFont;
    };
} & IContextTheme;
