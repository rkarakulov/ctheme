import { ISemanticFont } from './themeFont.interface';
export declare const smallSemanticFont: ISemanticFont;
export declare const mediumSemanticFont: ISemanticFont;
export declare const largeSemanticFont: ISemanticFont;
