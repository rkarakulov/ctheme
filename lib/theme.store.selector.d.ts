import { IThemeStore, IThemeStoreSegment } from './theme.store.interface';
export declare const themeStoreSelector: (store: IThemeStoreSegment) => IThemeStore;
