import { FontSize, PaletteName } from './theme.constant';
export declare namespace ThemeAction {
    const setPalette: import("typescript-fsa").ActionCreator<PaletteName>;
    const setFontSize: import("typescript-fsa").ActionCreator<FontSize>;
}
