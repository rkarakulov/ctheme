import { IThemeLocalData, IThemeStore } from './theme.store.interface';
export declare namespace ThemeLocalDataTranslator {
    function fromStore(state: IThemeStore): IThemeLocalData;
    function toStore(state: IThemeStore, localData: IThemeLocalData): IThemeStore;
}
