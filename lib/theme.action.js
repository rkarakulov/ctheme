"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var typescript_fsa_1 = require("typescript-fsa");
var ThemeAction;
(function (ThemeAction) {
    var actionCreator = typescript_fsa_1.default('Theme');
    ThemeAction.setPalette = actionCreator('SET_PALETTE');
    ThemeAction.setFontSize = actionCreator('SET_FONT_SIZE');
})(ThemeAction = exports.ThemeAction || (exports.ThemeAction = {}));
