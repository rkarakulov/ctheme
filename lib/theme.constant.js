"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FontSize;
(function (FontSize) {
    FontSize["Small"] = "Small";
    FontSize["Medium"] = "Medium";
    FontSize["Large"] = "Large";
})(FontSize = exports.FontSize || (exports.FontSize = {}));
var PaletteName;
(function (PaletteName) {
    PaletteName["Dark"] = "dark";
    PaletteName["Light"] = "light";
})(PaletteName = exports.PaletteName || (exports.PaletteName = {}));
var BackgroundType;
(function (BackgroundType) {
    BackgroundType[BackgroundType["Primary"] = 0] = "Primary";
    BackgroundType[BackgroundType["Secondary"] = 1] = "Secondary";
    BackgroundType[BackgroundType["Tertiary"] = 2] = "Tertiary";
    BackgroundType[BackgroundType["Positive"] = 3] = "Positive";
    BackgroundType[BackgroundType["Transparent"] = 4] = "Transparent";
})(BackgroundType = exports.BackgroundType || (exports.BackgroundType = {}));
exports.themeInitialState = {
    palette: PaletteName.Light,
    fontSize: FontSize.Small,
};
