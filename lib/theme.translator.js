"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var ThemeLocalDataTranslator;
(function (ThemeLocalDataTranslator) {
    function fromStore(state) {
        return {
            palette: state.palette,
            fontSize: state.fontSize,
        };
    }
    ThemeLocalDataTranslator.fromStore = fromStore;
    function toStore(state, localData) {
        return __assign({}, state, { palette: localData.palette, fontSize: localData.fontSize });
    }
    ThemeLocalDataTranslator.toStore = toStore;
})(ThemeLocalDataTranslator = exports.ThemeLocalDataTranslator || (exports.ThemeLocalDataTranslator = {}));
