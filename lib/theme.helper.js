"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var theme_constant_1 = require("./theme.constant");
var themeFont_constant_1 = require("./font/themeFont.constant");
function getSemanticFont(fontSize) {
    var semanticFont = {};
    if (fontSize === theme_constant_1.FontSize.Small) {
        semanticFont = themeFont_constant_1.smallSemanticFont;
    }
    else if (fontSize === theme_constant_1.FontSize.Medium) {
        semanticFont = themeFont_constant_1.mediumSemanticFont;
    }
    else if (fontSize === theme_constant_1.FontSize.Large) {
        semanticFont = themeFont_constant_1.largeSemanticFont;
    }
    return semanticFont;
}
exports.getSemanticFont = getSemanticFont;
function applyAlpha(color, alpha) {
    if (alpha !== undefined && alpha !== 1 && color !== 'transparent') {
        var _a = colorToRgb(color), r = _a.r, g = _a.g, b = _a.b;
        return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
    }
    else {
        return color;
    }
}
exports.applyAlpha = applyAlpha;
var rgb6Re = /#[a-fA-F0-9]{6}/;
var rgb3Re = /#[a-fA-F0-9]{3}/;
var rgbaRe = /rgba?\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(,\s*(0?\.\d+)\s*)?/;
function colorToRgb(color) {
    if (rgb6Re.test(color)) {
        var r = parseInt(color.slice(1, 3), 16);
        var g = parseInt(color.slice(3, 5), 16);
        var b = parseInt(color.slice(5, 7), 16);
        return {
            r: r,
            g: g,
            b: b,
        };
    }
    else if (rgb3Re.test(color)) {
        var r = parseInt(color[1] + color[1], 16);
        var g = parseInt(color[2] + color[2], 16);
        var b = parseInt(color[3] + color[3], 16);
        return {
            r: r,
            g: g,
            b: b,
        };
    }
    else {
        var match = color.match(rgbaRe);
        if (match) {
            var r = parseInt(match[1], 10);
            var g = parseInt(match[2], 10);
            var b = parseInt(match[3], 10);
            var a = parseFloat(match[5]);
            return isNaN(a)
                ? {
                    r: r,
                    g: g,
                    b: b,
                }
                : {
                    r: r,
                    g: g,
                    b: b,
                    a: a,
                };
        }
        else {
            throw new Error('color argument does not matches to any color pattern (hex or rgba)');
        }
    }
}
exports.colorToRgb = colorToRgb;
