"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var lightTooltip_style_1 = require("./lightTooltip.style");
var theme_helper_1 = require("../theme.helper");
exports.darkPrimaryForegroundColor = '#FFFFFF';
var disabledOpacity = 0.50;
var tertiaryForegroundColor = theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.40);
var menuBgColor = '#1A1A1A';
exports.darkPrimaryBackgroundColor = '#292929';
var secondaryBgColor = '#525252';
var muted1 = theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.35);
var muted2 = theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.20);
exports.darkSemanticPalette = {
    backgroundColor: {
        primaryNormal: exports.darkPrimaryBackgroundColor,
        primaryHover: '#363636',
        primaryDisabled: theme_helper_1.applyAlpha(exports.darkPrimaryBackgroundColor, disabledOpacity),
        secondaryNormal: secondaryBgColor,
        secondaryHover: '#5E5E5E',
        secondaryDisabled: theme_helper_1.applyAlpha(secondaryBgColor, disabledOpacity),
        tertiaryNormal: '#3B3B3B',
        tertiaryHover: '#454545',
        tertiaryDisabled: theme_helper_1.applyAlpha('#3B3B3B', disabledOpacity),
        quaternaryNormal: menuBgColor,
        quaternaryHover: '#212121',
        quaternaryDisabled: theme_helper_1.applyAlpha(menuBgColor, disabledOpacity),
        quinaryNormal: '#000000',
        menuNormal: menuBgColor,
        menuHover: '#363636',
        errorNormal: '#9f4747',
        errorHover: '#BA5353',
        errorDisabled: theme_helper_1.applyAlpha('#9f4747', disabledOpacity),
        warningNormal: '#F5A623',
        strokeNormal: theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.12),
    },
    disabledOpacity: disabledOpacity,
    buttonBackgroundColor: {
        positiveNormal: '#009345',
        positiveHover: '#10A651',
        positiveDisabled: theme_helper_1.applyAlpha('#009345', disabledOpacity),
        negativeNormal: '#F05824',
        negativeHover: '#FF6C36',
        negativeDisabled: theme_helper_1.applyAlpha('#F05824', disabledOpacity),
        tradeNormal: '#808080',
        tradeHover: '#8A8A8A',
        tradeDisabled: theme_helper_1.applyAlpha('#808080', disabledOpacity),
        primaryNormal: '#4A4A4A',
        primaryHover: '#595959',
        primaryToggled: '#3B3B3B',
        primaryDisabled: theme_helper_1.applyAlpha('#4A4A4A', disabledOpacity),
        secondaryNormal: '#6B6B6B',
        secondaryHover: '#858585',
        secondaryToggled: '#3B3B3B',
        secondaryDisabled: theme_helper_1.applyAlpha('#6B6B6B', disabledOpacity),
        tertiaryNormal: '#525252',
        tertiaryHover: '#5E5E5E',
        tertiaryToggled: '#292929',
        tertiaryDisabled: theme_helper_1.applyAlpha('#525252', disabledOpacity),
        greenButtonOnlyCopyApp: '#61995b',
        greenButtonOnlyCopyAppHover: '#4a7346',
        goLongAccent: '#FFFFFF',
        goShortAccent: '#FFFFFF',
    },
    foregroundColor: {
        positive: '#19B52E',
        negative: '#FF6600',
        primaryForAccentButtons: exports.darkPrimaryForegroundColor,
        primaryForAccentButtonsDisabled: theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, disabledOpacity),
        primary: exports.darkPrimaryForegroundColor,
        secondary: theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.70),
        tertiaryForBrandingBackground: tertiaryForegroundColor,
        tertiary: tertiaryForegroundColor,
        muted1: muted1,
        muted2: muted2,
        error: '#FF5252',
    },
    badgeBackgroundColor: {
        positive: '#009345',
        negative: '#F05824',
        info: '#0091EA',
        muted1: muted1,
        error: '#B71C1C',
    },
    shadow: {
        dialog: '0 5px 40px rgba(0, 0, 0, 1)',
        dropdownsAndTooltips: '0 0 4px rgba(0, 0, 0, 0.4)',
        scroll: 'rgba(0, 0, 0, 0.2)',
    },
    dialog: {
        header: {
            foregroundColor: {
                forContentPrimaryBackground: exports.darkPrimaryForegroundColor,
                forContentSecondaryBackground: exports.darkPrimaryForegroundColor,
            },
            backgroundColor: {
                forContentPrimaryBackground: menuBgColor,
                forContentSecondaryBackground: exports.darkPrimaryBackgroundColor,
            },
        },
        contentOverlay: {
            backgroundColor: theme_helper_1.applyAlpha('#2a2a2a', 0.9),
        },
    },
    tooltip: __assign({}, lightTooltip_style_1.lightTooltipStyles, { backgroundColor: menuBgColor, color: exports.darkPrimaryForegroundColor, borderColor: secondaryBgColor }),
    lightTooltip: lightTooltip_style_1.lightTooltipStyles,
    charts: {
        axises: {
            axis: '#b3b3b3',
        },
        equity: {
            balance: '#FFD600',
            equity: '#19B52E',
            minEquity: '#9E9E9E',
            maxEquity: '#9E9E9E',
            equityArea: '#9E9E9E',
        },
        bar: {
            positive: {
                normal: '#009247',
                hover: '#00ad54',
            },
        },
    },
    target: {
        pivotColor: '#3fa9f5',
    },
    account: {
        liveAccountColor: '#00c853',
        demoAccountColor: '#0f99ff',
    },
    mirror: {
        infoBlockSeparator: '#505050',
        foregroundColor: {
            secondary: '#a5a5a5',
        },
        chart: {
            strokeColor: '#41B655',
            gradientColor: '#019147',
        },
    },
    widget: {
        radio: {
            checked: '#00c853',
            checkedHover: '#00993f',
            checkedLight: theme_helper_1.applyAlpha('#00c853', 0.5),
            unchecked: theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.54),
            uncheckedDark: theme_helper_1.applyAlpha('#221f1f', 0.26),
            uncheckedLight: '#f1f1f1',
            uncheckedHover: theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.87),
        },
        colorPicker: {
            border: theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.54),
        },
        input: {
            prefix: theme_helper_1.applyAlpha(exports.darkPrimaryForegroundColor, 0.38),
        },
    },
    link: {
        normal: '#0091EA',
    },
    notification: {
        normal: '#000000',
    },
};
