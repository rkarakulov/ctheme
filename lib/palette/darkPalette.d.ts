import { ISemanticPalette } from './themePalette.interface';
export declare const darkPrimaryForegroundColor = "#FFFFFF";
export declare const darkPrimaryBackgroundColor = "#292929";
export declare const darkSemanticPalette: ISemanticPalette;
