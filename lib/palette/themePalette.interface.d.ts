export interface ITooltipStyles {
    backgroundColor: string;
    color: string;
    shadow: string;
    borderColor: string;
    tableBorderColor: string;
}
export interface IForegroundColor {
    positive: string;
    negative: string;
    primaryForAccentButtons: string;
    primaryForAccentButtonsDisabled: string;
    primary: string;
    secondary: string;
    tertiaryForBrandingBackground: string;
    tertiary: string;
    muted1: string;
    muted2: string;
    error: string;
}
export interface IButtonBackgroundColor {
    positiveNormal: string;
    positiveHover: string;
    positiveDisabled: string;
    negativeNormal: string;
    negativeHover: string;
    negativeDisabled: string;
    tradeNormal: string;
    tradeHover: string;
    tradeDisabled: string;
    primaryNormal: string;
    primaryHover: string;
    primaryDisabled: string;
    primaryToggled: string;
    secondaryNormal: string;
    secondaryHover: string;
    secondaryDisabled: string;
    secondaryToggled: string;
    tertiaryNormal: string;
    tertiaryHover: string;
    tertiaryDisabled: string;
    tertiaryToggled: string;
    greenButtonOnlyCopyApp: string;
    greenButtonOnlyCopyAppHover: string;
    goLongAccent: string;
    goShortAccent: string;
}
/**
 * Follow guide - https://yt.ctrader.com/issue/XD-1868
 */
export interface ISemanticPalette {
    backgroundColor: {
        primaryNormal: string;
        primaryHover: string;
        primaryDisabled: string;
        secondaryNormal: string;
        secondaryHover: string;
        secondaryDisabled: string;
        tertiaryNormal: string;
        tertiaryHover: string;
        tertiaryDisabled: string;
        quaternaryNormal: string;
        quaternaryHover: string;
        quaternaryDisabled: string;
        quinaryNormal: string;
        menuNormal: string;
        menuHover: string;
        errorNormal: string;
        errorHover: string;
        errorDisabled: string;
        warningNormal: string;
        strokeNormal: string;
    };
    disabledOpacity: number;
    buttonBackgroundColor: IButtonBackgroundColor;
    foregroundColor: IForegroundColor;
    badgeBackgroundColor: {
        positive: string;
        negative: string;
        info: string;
        muted1: string;
        error: string;
    };
    shadow: {
        dialog: string;
        scroll: string;
        dropdownsAndTooltips: string;
    };
    dialog: {
        header: {
            foregroundColor: {
                forContentPrimaryBackground: string;
                forContentSecondaryBackground: string;
            };
            backgroundColor: {
                forContentPrimaryBackground: string;
                forContentSecondaryBackground: string;
            };
        };
        contentOverlay: {
            backgroundColor: string;
        };
    };
    target: {
        pivotColor: string;
    };
    tooltip: ITooltipStyles;
    lightTooltip: ITooltipStyles;
    charts: {
        axises: {
            axis: string;
        };
        equity: {
            balance: string;
            equity: string;
            minEquity: string;
            maxEquity: string;
            equityArea: string;
        };
        bar: {
            positive: {
                normal: string;
                hover: string;
            };
        };
    };
    account: {
        liveAccountColor: string;
        demoAccountColor: string;
    };
    mirror: {
        infoBlockSeparator: string;
        foregroundColor: {
            secondary: string;
        };
        chart: {
            strokeColor: string;
            gradientColor: string;
        };
    };
    widget: {
        radio: {
            checked: string;
            checkedHover: string;
            checkedLight: string;
            unchecked: string;
            uncheckedDark: string;
            uncheckedLight: string;
            uncheckedHover: string;
        };
        colorPicker: {
            border: string;
        };
        input: {
            prefix: string;
        };
    };
    link: {
        normal: string;
    };
    notification: {
        normal: string;
    };
}
