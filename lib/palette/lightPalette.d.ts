import { ISemanticPalette } from './themePalette.interface';
export declare const lightPrimaryBackgroundColor = "#FFFFFF";
export declare const lightPrimaryForegroundColor: string;
export declare const lightSemanticPalette: ISemanticPalette;
