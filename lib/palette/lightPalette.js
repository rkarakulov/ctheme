"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var lightTooltip_style_1 = require("./lightTooltip.style");
var theme_helper_1 = require("../theme.helper");
var textIconColor = '#000000';
var disabledOpacity = 0.50;
var menuBgColor = '#E7EBED';
exports.lightPrimaryBackgroundColor = '#FFFFFF';
var secondaryBgColor = '#D5DDE1';
exports.lightPrimaryForegroundColor = theme_helper_1.applyAlpha(textIconColor, 0.90);
exports.lightSemanticPalette = {
    backgroundColor: {
        primaryNormal: exports.lightPrimaryBackgroundColor,
        primaryHover: '#F5F8FA',
        primaryDisabled: theme_helper_1.applyAlpha(exports.lightPrimaryBackgroundColor, disabledOpacity),
        secondaryNormal: secondaryBgColor,
        secondaryHover: '#CDD5D9',
        secondaryDisabled: theme_helper_1.applyAlpha(secondaryBgColor, disabledOpacity),
        tertiaryNormal: '#ECEFF1',
        tertiaryHover: '#E0E4E6',
        tertiaryDisabled: theme_helper_1.applyAlpha('#ECEFF1', disabledOpacity),
        quaternaryNormal: menuBgColor,
        quaternaryHover: '#D6DADC',
        quaternaryDisabled: theme_helper_1.applyAlpha(menuBgColor, disabledOpacity),
        quinaryNormal: '#000000',
        menuNormal: menuBgColor,
        menuHover: '#FAFAFA',
        errorNormal: '#FF5252',
        errorHover: '#E64A4A',
        errorDisabled: theme_helper_1.applyAlpha('#FF5252', disabledOpacity),
        warningNormal: '#F5A623',
        strokeNormal: theme_helper_1.applyAlpha(textIconColor, 0.12),
    },
    disabledOpacity: disabledOpacity,
    buttonBackgroundColor: {
        positiveNormal: '#009345',
        positiveHover: '#0C7A3C',
        positiveDisabled: theme_helper_1.applyAlpha('#009345', disabledOpacity),
        negativeNormal: '#F05824',
        negativeHover: '#D94F21',
        negativeDisabled: theme_helper_1.applyAlpha('#F05824', disabledOpacity),
        tradeNormal: '#B0BEC5',
        tradeHover: '#90A4AE',
        tradeDisabled: theme_helper_1.applyAlpha('#B0BEC5', disabledOpacity),
        primaryNormal: '#ECEFF1',
        primaryHover: '#E7EBED',
        primaryToggled: '#D5DDE1',
        primaryDisabled: theme_helper_1.applyAlpha('#ECEFF1', disabledOpacity),
        secondaryNormal: '#ECEFF1',
        secondaryHover: '#E7EBED',
        secondaryToggled: '#FFFFFF',
        secondaryDisabled: theme_helper_1.applyAlpha('#ECEFF1', disabledOpacity),
        tertiaryNormal: '#FFFFFF',
        tertiaryHover: '#C4CED3',
        tertiaryToggled: '#D5DDE1',
        tertiaryDisabled: theme_helper_1.applyAlpha('#FFFFFF', disabledOpacity),
        greenButtonOnlyCopyApp: '#5e9960',
        greenButtonOnlyCopyAppHover: '#477348',
        goLongAccent: '#FFFFFF',
        goShortAccent: '#FFFFFF',
    },
    foregroundColor: {
        positive: '#19B52E',
        negative: '#FF6600',
        primaryForAccentButtons: '#FFFFFF',
        primaryForAccentButtonsDisabled: theme_helper_1.applyAlpha('#FFFFFF', disabledOpacity),
        primary: exports.lightPrimaryForegroundColor,
        secondary: theme_helper_1.applyAlpha(textIconColor, 0.65),
        tertiaryForBrandingBackground: theme_helper_1.applyAlpha('#FFFFFF', 0.40),
        tertiary: theme_helper_1.applyAlpha(textIconColor, 0.40),
        muted1: theme_helper_1.applyAlpha(textIconColor, 0.20),
        muted2: theme_helper_1.applyAlpha(textIconColor, 0.15),
        error: '#FF5252',
    },
    badgeBackgroundColor: {
        positive: '#009345',
        negative: '#F05824',
        info: '#0091EA',
        muted1: theme_helper_1.applyAlpha(textIconColor, 0.20),
        error: '#FF5252',
    },
    shadow: {
        dialog: '0 5px 40px rgba(0, 0, 0, 0.5)',
        dropdownsAndTooltips: '0 0 4px rgba(0, 0, 0, 0.2)',
        scroll: 'rgba(0, 0, 0, 0.1)',
    },
    dialog: {
        header: {
            foregroundColor: {
                forContentPrimaryBackground: textIconColor,
                forContentSecondaryBackground: textIconColor,
            },
            backgroundColor: {
                forContentPrimaryBackground: secondaryBgColor,
                forContentSecondaryBackground: exports.lightPrimaryBackgroundColor,
            },
        },
        contentOverlay: {
            backgroundColor: theme_helper_1.applyAlpha('#e7ebed', 0.9),
        },
    },
    tooltip: __assign({}, lightTooltip_style_1.lightTooltipStyles, { backgroundColor: menuBgColor, color: textIconColor, borderColor: secondaryBgColor }),
    lightTooltip: lightTooltip_style_1.lightTooltipStyles,
    charts: {
        axises: {
            axis: '#000',
        },
        equity: {
            balance: '#FFD600',
            equity: '#19B52E',
            minEquity: '#9E9E9E',
            maxEquity: '#9E9E9E',
            equityArea: '#9E9E9E',
        },
        bar: {
            positive: {
                normal: '#009247',
                hover: '#00ad54',
            },
        },
    },
    target: {
        pivotColor: '#3fa9f5',
    },
    account: {
        liveAccountColor: '#329337',
        demoAccountColor: '#0772dd',
    },
    mirror: {
        infoBlockSeparator: '#ccc',
        foregroundColor: {
            secondary: '#939393',
        },
        chart: {
            strokeColor: '#41B655',
            gradientColor: '#019147',
        },
    },
    widget: {
        radio: {
            checked: '#00c853',
            checkedHover: '#00993f',
            checkedLight: theme_helper_1.applyAlpha('#00c853', 0.5),
            unchecked: theme_helper_1.applyAlpha(textIconColor, 0.54),
            uncheckedDark: theme_helper_1.applyAlpha('#221f1f', 0.26),
            uncheckedLight: '#f1f1f1',
            uncheckedHover: theme_helper_1.applyAlpha(textIconColor, 0.87),
        },
        colorPicker: {
            border: theme_helper_1.applyAlpha(textIconColor, 0.54),
        },
        input: {
            prefix: theme_helper_1.applyAlpha(textIconColor, 0.38),
        },
    },
    link: {
        normal: '#0091EA',
    },
    notification: {
        normal: '#ffffff',
    },
};
