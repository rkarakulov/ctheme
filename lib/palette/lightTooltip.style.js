"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lightTooltipStyles = {
    backgroundColor: '#f5f5f5',
    color: '#000',
    shadow: '0 0 4px rgba(0, 0, 0, 0.2)',
    borderColor: '#808080',
    tableBorderColor: '#a0a0a0',
};
