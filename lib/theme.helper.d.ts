import { FontSize } from './theme.constant';
import { ISemanticFont } from './font/themeFont.interface';
export declare function getSemanticFont(fontSize: FontSize): ISemanticFont;
interface IRGBA {
    r: number;
    g: number;
    b: number;
    a?: number;
}
export declare function applyAlpha(color: string, alpha?: number): string;
export declare function colorToRgb(color: string): IRGBA;
export {};
