import { IThemeStore } from './theme.store.interface';
export declare enum FontSize {
    Small = "Small",
    Medium = "Medium",
    Large = "Large"
}
export declare enum PaletteName {
    Dark = "dark",
    Light = "light"
}
export declare enum BackgroundType {
    Primary = 0,
    Secondary = 1,
    Tertiary = 2,
    Positive = 3,
    Transparent = 4
}
export declare const themeInitialState: IThemeStore;
