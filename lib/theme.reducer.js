"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typescript_fsa_reducers_1 = require("typescript-fsa-reducers");
var theme_action_1 = require("./theme.action");
var theme_constant_1 = require("./theme.constant");
exports.themeReducer = typescript_fsa_reducers_1.reducerWithInitialState(theme_constant_1.themeInitialState)
    .case(theme_action_1.ThemeAction.setPalette, function (state, palette) {
    return __assign({}, state, { palette: palette });
})
    .case(theme_action_1.ThemeAction.setFontSize, function (state, fontSize) {
    return __assign({}, state, { fontSize: fontSize });
});
