import {IThemeStore} from './theme.store.interface';

export enum FontSize {
    Small = 'Small',
    Medium = 'Medium',
    Large = 'Large'
}

export enum PaletteName {
    Dark = 'dark',
    Light = 'light',
}

export enum BackgroundType {
    Primary,
    Secondary,
    Tertiary,
    Positive,
    Transparent
}

export const themeInitialState: IThemeStore = {
    palette: PaletteName.Light,
    fontSize: FontSize.Small,
};
