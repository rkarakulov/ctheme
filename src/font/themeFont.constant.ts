import {ISemanticFont} from './themeFont.interface';

export const smallSemanticFont: ISemanticFont = {
    size: {
        primary: 11,
        secondary: 13,
        tertiary: 15,
    },
    title: {
        primary: 18,
        secondary: 20,
        tertiary: 23,
    },
    buttonSize: {
        primary: 11,
        secondary: 13,
        tertiary: 15,
    },
    chart: {
        primary: 10,
    },
};

export const mediumSemanticFont: ISemanticFont = {
    size: {
        primary: 12,
        secondary: 14,
        tertiary: 16,
    },
    title: {
        primary: 19,
        secondary: 21,
        tertiary: 24,
    },
    buttonSize: {
        primary: 12,
        secondary: 14,
        tertiary: 16,
    },
    chart: {
        primary: 11,
    },
};

export const largeSemanticFont: ISemanticFont = {
    size: {
        primary: 13,
        secondary: 15,
        tertiary: 17,
    },
    title: {
        primary: 20,
        secondary: 22,
        tertiary: 25,
    },
    buttonSize: {
        primary: 13,
        secondary: 15,
        tertiary: 17,
    },
    chart: {
        primary: 12,
    },
};
