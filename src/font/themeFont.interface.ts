export interface ISemanticFont {
    size: {
        primary: number;
        secondary: number;
        tertiary: number;
    };
    title: {
        primary: number;
        secondary: number;
        tertiary: number;
    };
    buttonSize: {
        primary: number;
        secondary: number;
        tertiary: number;
    };
    chart: {
        primary: number;
    };
}
