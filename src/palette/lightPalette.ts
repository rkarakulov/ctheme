import {ISemanticPalette} from './themePalette.interface';
import {lightTooltipStyles} from './lightTooltip.style';
import {applyAlpha} from '../theme.helper';

const textIconColor = '#000000';
const disabledOpacity = 0.50;
const menuBgColor = '#E7EBED';
export const lightPrimaryBackgroundColor = '#FFFFFF';
const secondaryBgColor = '#D5DDE1';

export const lightPrimaryForegroundColor = applyAlpha(textIconColor, 0.90);

export const lightSemanticPalette: ISemanticPalette = {
    backgroundColor: {
        primaryNormal: lightPrimaryBackgroundColor,
        primaryHover: '#F5F8FA',
        primaryDisabled: applyAlpha(lightPrimaryBackgroundColor, disabledOpacity),

        secondaryNormal: secondaryBgColor,
        secondaryHover: '#CDD5D9',
        secondaryDisabled: applyAlpha(secondaryBgColor, disabledOpacity),

        tertiaryNormal: '#ECEFF1',
        tertiaryHover: '#E0E4E6',
        tertiaryDisabled: applyAlpha('#ECEFF1', disabledOpacity),

        quaternaryNormal: menuBgColor,
        quaternaryHover: '#D6DADC',
        quaternaryDisabled: applyAlpha(menuBgColor, disabledOpacity),

        quinaryNormal: '#000000',

        menuNormal: menuBgColor,
        menuHover: '#FAFAFA',

        errorNormal: '#FF5252',
        errorHover: '#E64A4A',
        errorDisabled: applyAlpha('#FF5252', disabledOpacity),
        warningNormal: '#F5A623',

        strokeNormal: applyAlpha(textIconColor, 0.12),
    },

    disabledOpacity,

    buttonBackgroundColor: {
        positiveNormal: '#009345',
        positiveHover: '#0C7A3C',
        positiveDisabled: applyAlpha('#009345', disabledOpacity),

        negativeNormal: '#F05824',
        negativeHover: '#D94F21',
        negativeDisabled: applyAlpha('#F05824', disabledOpacity),

        tradeNormal: '#B0BEC5',
        tradeHover: '#90A4AE',
        tradeDisabled: applyAlpha('#B0BEC5', disabledOpacity),

        primaryNormal: '#ECEFF1',
        primaryHover: '#E7EBED',
        primaryToggled: '#D5DDE1',
        primaryDisabled: applyAlpha('#ECEFF1', disabledOpacity),

        secondaryNormal: '#ECEFF1',
        secondaryHover: '#E7EBED',
        secondaryToggled: '#FFFFFF',
        secondaryDisabled: applyAlpha('#ECEFF1', disabledOpacity),

        tertiaryNormal: '#FFFFFF',
        tertiaryHover: '#C4CED3',
        tertiaryToggled: '#D5DDE1',
        tertiaryDisabled: applyAlpha('#FFFFFF', disabledOpacity),

        greenButtonOnlyCopyApp: '#5e9960',
        greenButtonOnlyCopyAppHover: '#477348',
        goLongAccent: '#FFFFFF',
        goShortAccent: '#FFFFFF',
    },

    foregroundColor: {
        positive: '#19B52E',
        negative: '#FF6600',

        primaryForAccentButtons: '#FFFFFF',
        primaryForAccentButtonsDisabled: applyAlpha('#FFFFFF', disabledOpacity),

        primary: lightPrimaryForegroundColor,
        secondary: applyAlpha(textIconColor, 0.65),
        tertiaryForBrandingBackground: applyAlpha('#FFFFFF', 0.40),
        tertiary: applyAlpha(textIconColor, 0.40),
        muted1: applyAlpha(textIconColor, 0.20),
        muted2: applyAlpha(textIconColor, 0.15),
        error: '#FF5252',
    },

    badgeBackgroundColor: {
        positive: '#009345',
        negative: '#F05824',
        info: '#0091EA',
        muted1: applyAlpha(textIconColor, 0.20),
        error: '#FF5252',
    },

    shadow: {
        dialog: '0 5px 40px rgba(0, 0, 0, 0.5)',
        dropdownsAndTooltips: '0 0 4px rgba(0, 0, 0, 0.2)',
        scroll: 'rgba(0, 0, 0, 0.1)',
    },

    dialog: {
        header: {
            foregroundColor: {
                forContentPrimaryBackground: textIconColor,
                forContentSecondaryBackground: textIconColor,
            },

            backgroundColor: {
                forContentPrimaryBackground: secondaryBgColor,
                forContentSecondaryBackground: lightPrimaryBackgroundColor,
            },
        },
        contentOverlay: {
            backgroundColor: applyAlpha('#e7ebed', 0.9),
        },
    },

    tooltip: {
        ...lightTooltipStyles,
        backgroundColor: menuBgColor,
        color: textIconColor,
        borderColor: secondaryBgColor,
    },
    lightTooltip: lightTooltipStyles,

    charts: {
        axises: {
            axis: '#000',
        },
        equity: {
            balance: '#FFD600',
            equity: '#19B52E',
            minEquity: '#9E9E9E',
            maxEquity: '#9E9E9E',
            equityArea: '#9E9E9E',
        },
        bar: {
            positive: {
                normal: '#009247',
                hover: '#00ad54',
            },
        },
    },

    target: {
        pivotColor: '#3fa9f5',
    },

    account: {
        liveAccountColor: '#329337',
        demoAccountColor: '#0772dd',
    },

    mirror: {
        infoBlockSeparator: '#ccc',
        foregroundColor: {
            secondary: '#939393',
        },
        chart: {
            strokeColor: '#41B655',
            gradientColor: '#019147',
        },
    },

    widget: {
        radio: {
            checked: '#00c853',
            checkedHover: '#00993f',
            checkedLight: applyAlpha('#00c853', 0.5),
            unchecked: applyAlpha(textIconColor, 0.54),
            uncheckedDark: applyAlpha('#221f1f', 0.26),
            uncheckedLight: '#f1f1f1',
            uncheckedHover: applyAlpha(textIconColor, 0.87),
        },
        colorPicker: {
            border: applyAlpha(textIconColor, 0.54),
        },
        input: {
            prefix: applyAlpha(textIconColor, 0.38),
        },
    },

    link: {
        normal: '#0091EA',
    },

    notification: {
        normal: '#ffffff',
    },
};
