import {ITooltipStyles} from './themePalette.interface';

export const lightTooltipStyles: ITooltipStyles = {
    backgroundColor: '#f5f5f5',
    color: '#000',
    shadow: '0 0 4px rgba(0, 0, 0, 0.2)',
    borderColor: '#808080',
    tableBorderColor: '#a0a0a0',
};
