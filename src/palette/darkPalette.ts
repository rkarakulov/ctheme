import {ISemanticPalette} from './themePalette.interface';
import {lightTooltipStyles} from './lightTooltip.style';
import {applyAlpha} from '../theme.helper';

export const darkPrimaryForegroundColor = '#FFFFFF';
const disabledOpacity = 0.50;
const tertiaryForegroundColor = applyAlpha(darkPrimaryForegroundColor, 0.40);
const menuBgColor = '#1A1A1A';
export const darkPrimaryBackgroundColor = '#292929';
const secondaryBgColor = '#525252';

const muted1 = applyAlpha(darkPrimaryForegroundColor, 0.35);
const muted2 = applyAlpha(darkPrimaryForegroundColor, 0.20);

export const darkSemanticPalette: ISemanticPalette = {
    backgroundColor: {
        primaryNormal: darkPrimaryBackgroundColor,
        primaryHover: '#363636',
        primaryDisabled: applyAlpha(darkPrimaryBackgroundColor, disabledOpacity),

        secondaryNormal: secondaryBgColor,
        secondaryHover: '#5E5E5E',
        secondaryDisabled: applyAlpha(secondaryBgColor, disabledOpacity),

        tertiaryNormal: '#3B3B3B',
        tertiaryHover: '#454545',
        tertiaryDisabled: applyAlpha('#3B3B3B', disabledOpacity),

        quaternaryNormal: menuBgColor,
        quaternaryHover: '#212121',
        quaternaryDisabled: applyAlpha(menuBgColor, disabledOpacity),

        quinaryNormal: '#000000',

        menuNormal: menuBgColor,
        menuHover: '#363636',

        errorNormal: '#9f4747',
        errorHover: '#BA5353',
        errorDisabled: applyAlpha('#9f4747', disabledOpacity),
        warningNormal: '#F5A623',

        strokeNormal: applyAlpha(darkPrimaryForegroundColor, 0.12),
    },

    disabledOpacity,

    buttonBackgroundColor: {
        positiveNormal: '#009345',
        positiveHover: '#10A651',
        positiveDisabled: applyAlpha('#009345', disabledOpacity),

        negativeNormal: '#F05824',
        negativeHover: '#FF6C36',
        negativeDisabled: applyAlpha('#F05824', disabledOpacity),

        tradeNormal: '#808080',
        tradeHover: '#8A8A8A',
        tradeDisabled: applyAlpha('#808080', disabledOpacity),

        primaryNormal: '#4A4A4A',
        primaryHover: '#595959',
        primaryToggled: '#3B3B3B',
        primaryDisabled: applyAlpha('#4A4A4A', disabledOpacity),

        secondaryNormal: '#6B6B6B',
        secondaryHover: '#858585',
        secondaryToggled: '#3B3B3B',
        secondaryDisabled: applyAlpha('#6B6B6B', disabledOpacity),

        tertiaryNormal: '#525252',
        tertiaryHover: '#5E5E5E',
        tertiaryToggled: '#292929',
        tertiaryDisabled: applyAlpha('#525252', disabledOpacity),

        greenButtonOnlyCopyApp: '#61995b',
        greenButtonOnlyCopyAppHover: '#4a7346',
        goLongAccent: '#FFFFFF',
        goShortAccent: '#FFFFFF',
    },

    foregroundColor: {
        positive: '#19B52E',
        negative: '#FF6600',

        primaryForAccentButtons: darkPrimaryForegroundColor,
        primaryForAccentButtonsDisabled: applyAlpha(darkPrimaryForegroundColor, disabledOpacity),

        primary: darkPrimaryForegroundColor,
        secondary: applyAlpha(darkPrimaryForegroundColor, 0.70),
        tertiaryForBrandingBackground: tertiaryForegroundColor,
        tertiary: tertiaryForegroundColor,
        muted1,
        muted2,
        error: '#FF5252',
    },

    badgeBackgroundColor: {
        positive: '#009345',
        negative: '#F05824',
        info: '#0091EA',
        muted1,
        error: '#B71C1C',
    },

    shadow: {
        dialog: '0 5px 40px rgba(0, 0, 0, 1)',
        dropdownsAndTooltips: '0 0 4px rgba(0, 0, 0, 0.4)',
        scroll: 'rgba(0, 0, 0, 0.2)',
    },

    dialog: {
        header: {
            foregroundColor: {
                forContentPrimaryBackground: darkPrimaryForegroundColor,
                forContentSecondaryBackground: darkPrimaryForegroundColor,
            },
            backgroundColor: {
                forContentPrimaryBackground: menuBgColor,
                forContentSecondaryBackground: darkPrimaryBackgroundColor,
            },
        },
        contentOverlay: {
            backgroundColor: applyAlpha('#2a2a2a', 0.9),
        },
    },

    tooltip: {
        ...lightTooltipStyles,
        backgroundColor: menuBgColor,
        color: darkPrimaryForegroundColor,
        borderColor: secondaryBgColor,
    },
    lightTooltip: lightTooltipStyles,

    charts: {
        axises: {
            axis: '#b3b3b3',
        },
        equity: {
            balance: '#FFD600',
            equity: '#19B52E',
            minEquity: '#9E9E9E',
            maxEquity: '#9E9E9E',
            equityArea: '#9E9E9E',
        },
        bar: {
            positive: {
                normal: '#009247',
                hover: '#00ad54',
            },
        },
    },

    target: {
        pivotColor: '#3fa9f5',
    },

    account: {
        liveAccountColor: '#00c853',
        demoAccountColor: '#0f99ff',
    },

    mirror: {
        infoBlockSeparator: '#505050',
        foregroundColor: {
            secondary: '#a5a5a5',
        },
        chart: {
            strokeColor: '#41B655',
            gradientColor: '#019147',
        },
    },

    widget: {
        radio: {
            checked: '#00c853',
            checkedHover: '#00993f',
            checkedLight: applyAlpha('#00c853', 0.5),
            unchecked: applyAlpha(darkPrimaryForegroundColor, 0.54),
            uncheckedDark: applyAlpha('#221f1f', 0.26),
            uncheckedLight: '#f1f1f1',
            uncheckedHover: applyAlpha(darkPrimaryForegroundColor, 0.87),
        },
        colorPicker: {
            border: applyAlpha(darkPrimaryForegroundColor, 0.54),
        },
        input: {
            prefix: applyAlpha(darkPrimaryForegroundColor, 0.38),
        },
    },

    link: {
        normal: '#0091EA',
    },

    notification: {
        normal: '#000000',
    },
};
