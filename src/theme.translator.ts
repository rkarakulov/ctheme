import {IThemeLocalData, IThemeStore} from './theme.store.interface';

export namespace ThemeLocalDataTranslator {
    export function fromStore(state: IThemeStore): IThemeLocalData {
        return {
            palette: state.palette,
            fontSize: state.fontSize,
        };
    }

    export function toStore(state: IThemeStore, localData: IThemeLocalData): IThemeStore {
        return {
            ...state,
            palette: localData.palette,
            fontSize: localData.fontSize,
        };
    }
}
