import {reducerWithInitialState} from 'typescript-fsa-reducers';
import {ThemeAction} from './theme.action';
import {themeInitialState} from './theme.constant';

export const themeReducer = reducerWithInitialState(themeInitialState)
    .case(ThemeAction.setPalette, (state, palette) => {
        return {
            ...state,
            palette,
        };
    })
    .case(ThemeAction.setFontSize, (state, fontSize) => {
        return {
            ...state,
            fontSize,
        };
    });
