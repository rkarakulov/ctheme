import {FontSize} from './theme.constant';
import {smallSemanticFont, mediumSemanticFont, largeSemanticFont} from './font/themeFont.constant';
import {ISemanticFont} from './font/themeFont.interface';

export function getSemanticFont(fontSize: FontSize): ISemanticFont {
    let semanticFont = {} as ISemanticFont;

    if (fontSize === FontSize.Small) {
        semanticFont = smallSemanticFont;
    }
    else if (fontSize === FontSize.Medium) {
        semanticFont = mediumSemanticFont;
    }
    else if (fontSize === FontSize.Large) {
        semanticFont = largeSemanticFont;
    }

    return semanticFont;
}

interface IRGBA {
    r: number;
    g: number;
    b: number;
    a?: number;
}

export function applyAlpha(color: string, alpha?: number): string {
    if (alpha !== undefined && alpha !== 1 && color !== 'transparent') {
        const {r, g, b} = colorToRgb(color);
        return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
    }
    else {
        return color;
    }
}

const rgb6Re = /#[a-fA-F0-9]{6}/;
const rgb3Re = /#[a-fA-F0-9]{3}/;
const rgbaRe = /rgba?\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(,\s*(0?\.\d+)\s*)?/;

export function colorToRgb(color: string): IRGBA {
    if (rgb6Re.test(color)) {
        const r = parseInt(color.slice(1, 3), 16);
        const g = parseInt(color.slice(3, 5), 16);
        const b = parseInt(color.slice(5, 7), 16);
        return {
            r,
            g,
            b,
        };
    }
    else if (rgb3Re.test(color)) {
        const r = parseInt(color[1] + color[1], 16);
        const g = parseInt(color[2] + color[2], 16);
        const b = parseInt(color[3] + color[3], 16);
        return {
            r,
            g,
            b,
        };
    }
    else {
        const match = color.match(rgbaRe);
        if (match) {
            const r = parseInt(match[1], 10);
            const g = parseInt(match[2], 10);
            const b = parseInt(match[3], 10);
            const a = parseFloat(match[5]);
            return isNaN(a)
                ? {
                    r,
                    g,
                    b,
                }
                : {
                    r,
                    g,
                    b,
                    a,
                };
        }
        else {
            throw new Error('color argument does not matches to any color pattern (hex or rgba)');
        }
    }
}
