import {PaletteName, FontSize} from './theme.constant';

export interface IThemeStore {
    palette: PaletteName;
    fontSize: FontSize;
}

export interface IThemeStoreSegment {
    theme: IThemeStore;
}

export interface IThemeLocalData {
    palette: PaletteName;
    fontSize: FontSize;
}

export interface IThemeLocalDataSegment {
    theme: IThemeLocalData;
}
