import {IThemeStore, IThemeStoreSegment} from './theme.store.interface';

export const themeStoreSelector = (store: IThemeStoreSegment): IThemeStore => store.theme;
