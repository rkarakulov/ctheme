import {ThemeAction} from '../theme.action';
import {PaletteName, FontSize} from '../theme.constant';
import {themeReducer} from '../theme.reducer';
import {IThemeStore} from '../theme.store.interface';

describe('theme.reducer', () => {
    test('should set palette', () => {
        const actual = themeReducer({
            palette: PaletteName.Dark,
        } as IThemeStore, ThemeAction.setPalette(PaletteName.Light));
        const expected = {
            palette: PaletteName.Light,
        };

        expect(actual)
            .toEqual(expected);
    });

    test('should set font size', () => {
        const actual = themeReducer({
            fontSize: FontSize.Medium,
        } as IThemeStore, ThemeAction.setFontSize(FontSize.Small));
        const expected = {
            fontSize: FontSize.Small,
        };

        expect(actual)
            .toEqual(expected);
    });
});
