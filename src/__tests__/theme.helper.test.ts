import {colorToRgb, applyAlpha} from '../theme.helper';

describe('theme.helper', () => {
    const color = '#ffffff';

    describe('colorToRgb', () => {
        const expectedRgb = {
            r: 255,
            g: 255,
            b: 255,
        };

        test('Should return rgb of short hex', () => {
            const shortColor = '#fff';
            const res = colorToRgb(shortColor);
            expect(res)
                .toEqual(expectedRgb);
        });

        test('Should return rgb of hex', () => {
            const res = colorToRgb(color);
            expect(res)
                .toEqual(expectedRgb);
        });

        test('Should return rgb of rgba', () => {
            const rgbaColor = 'rgba(255, 255, 255, 0)';
            const res = colorToRgb(rgbaColor);
            expect(res)
                .toEqual(expectedRgb);
        });

        test('Should return rgb of rgb', () => {
            const rgbColor = 'rgb(255, 255, 255)';
            const res = colorToRgb(rgbColor);
            expect(res)
                .toEqual(expectedRgb);
        });

        test('Should trow error if incorrect value passed', () => {
            const incorrectColor = 'incorrect';
            expect(() => colorToRgb(incorrectColor))
                .toThrow();
        });
    });

    describe('applyAlpha', () => {
        test('Should return the passed color if alpha is not passed', () => {
            const res = applyAlpha(color);
            expect(res)
                .toEqual(color);
        });

        test('Should return the passed color if alpha is 1', () => {
            const res = applyAlpha(color, 1);
            expect(res)
                .toEqual(color);
        });

        test('Should return the passed color if its value is "transparent"', () => {
            const res = applyAlpha('transparent', 0.7);
            expect(res)
                .toEqual('transparent');
        });

        test('Should return the rgba representation of hex value', () => {
            const res = applyAlpha(color, 0.7);
            expect(res)
                .toEqual('rgba(255, 255, 255, 0.7)');
        });
    });
});
