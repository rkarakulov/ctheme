import typescriptFsa from 'typescript-fsa';
import {FontSize, PaletteName} from './theme.constant';

export namespace ThemeAction {
    const actionCreator = typescriptFsa('Theme');

    export const setPalette = actionCreator<PaletteName>('SET_PALETTE');
    export const setFontSize = actionCreator<FontSize>('SET_FONT_SIZE');
}
